/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects.
Entities:
    InheritanceDemo.
    
Function Signature:
    public static void main(String[] args)
    
Jobs to be Done:
    1)Create the object dog for Dog class.
    2)Create the object snake for Snake class.
    3)Create the object cat for Cat class.
    4)Create the object animal for Animal class.
    5)Call the each method using the object .
*/


package com.kpr.training.inheritance;

public class InheritanceDemo {
    
    public static void main(String[] args) {
        Dog dog = new Dog();
        Snake snake = new Snake();
        Cat cat = new Cat();
        Animal animal = new Animal();
        dog.sound();
        snake.sound();
        cat.sound();
        animal.sound();
        animal.moves();
        animal.moves(5);


    }
}
