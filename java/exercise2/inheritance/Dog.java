/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects .
Entities:
    Dog extends Animal

Function Signature:
    public void sound().

Jobs to be done:
   
    1) Declare the method sound().
    2) Print the the statement.
*/

package com.kpr.training.inheritance;

public class Dog extends Animal {
    
    public void sound() {
        System.out.println("The dog barks");
    }
}
