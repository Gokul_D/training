/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects.

Entities:
    Animal

Function Signature:
    public void sound()
    public void moves()
    public void moves(int kilometer)

Jobs to be Done:
    1) Declare the method sound.
        1.1)print the statement.
    2) Declare the method moves.
        2.1)print the statement.
    3) Declare the method moves with parameter 
        3.1)print the statement.
 */

package com.kpr.training.inheritance;

public class Animal {
    
    public void sound() {
        System.out.println(" This is the parent class");
    }

    public void moves() {
        System.out.println("The animal is moving");
    }

    public void moves(int kilometer) {
        System.out.println("The animal moves without any tired");
    }
}
