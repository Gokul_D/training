/*
Requirement:
    To compare the enum values using equals method and == operator.

Entity:
    EnumDemo.

Function Signature:
    public static void main(String[] args)

Jobs to be Done:
    1)Create a variable and assign null.
    2)Create a enum type Mentor and store the values.
    3)Create a enum type Student and store the values.
    4)Compare using "==" operator.
        4.1)Check if the two Strings point to the same address,then print true.
        4.2)otherwise print false.
    5)Compare using equals.
        5.1)check if the two strings are equal,then print true.
        5.2)otherwise print false.
    6)Add a new element to the enum Student.
    7)Compare using "==" operator.
        7.1)Check if the two Strings point to the same address,then print true.
        7.2)otherwise print false.
    8)Compare using equals.
        8.1)check if the two strings are equal,then print true.
        8.2)otherwise print false.
    
*/


//Answer:

package com.kpr.training.Enum;

public class EnumDemo {

    public enum Mentor {
        Raja, Kali, Vishnu, Thiyagu
    }

    public enum Student {
        Ravi, Karthi, Boobalan, Gokul, Kiruthic
    }

    public static void main(String[] args) {
        Mentor mentor = null;
        System.out.println(mentor == Mentor.Raja); // prints false
        System.out.println(Student.Ravi.equals(Mentor.Raja)); // prints false
        Student student = Student.Ravi;
        System.out.println(student == Student.Ravi); // prints true
        System.out.println(student.equals(Student.Ravi)); // prints true
    }
}
