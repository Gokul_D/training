/*
 * Requirement:
 *      To demonstrate the abstract class using the class shape that has two methods to
 * calculate the area and perimeter of two classes named the Square and the Circle extended from the
 * class Shape.
 * 
 * Entity:
 *      Square extends Shape
 * 
 * Method Signature:
 *      public void printArea()
 *      public void print Perimeter()
 * 
 * Jobs To Be Done:
 *      1) Declare a privete variable.
 *      2)Declare a constructor with a parameter.
 *              2.1)Assign the parameter value to the variable.
 *      3)Declare a method.
 *              3.1)Print the area of the square.
 *      4)Declare a method.
 *              4.1)Print the perimeter of square.
 */
package com.kpr.training.abstractDemo;

public class Square extends Shape {

    private double sideLength;

    public Square(double sideLength) {
        this.sideLength = sideLength;
    }

    public void printArea() {
        System.out.println(sideLength * sideLength);
    }

    public void printPerimeter() {
        System.out.println(4 * sideLength);
    }
}