/*
 * Requirement:
 *      To demonstrate the abstract class using the class shape that has two methods to
 * calculate the area and perimeter of two classes named the Square and the Circle extended from the
 * class Shape.
 *
 * Entity:
 *      Circle extends Shape
 *
 * Method Signature:
 *      public void printArea()
 *      public void print Perimeter()
 *
 * Jobs To Be Done:
 *      1)Declare the variable radius.
 *      2)Declare a constructor with one parameter as type double.
 *              2.1)Assign the parameter value to radius.
 *      3)Declare a method.
 *              3.1)Print the area of circle.
 *      4)Declare a method.
 *              4.1)Print the perimeter of the circle.
 *
 */
package com.kpr.training.abstractDemo;

public class Circle extends Shape {

    private final double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void printArea() {
        System.out.println(3.14 * radius * radius);
    }

    public void printPerimeter() {
        System.out.println(2 * 3.14 * radius);
    }
}