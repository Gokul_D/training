/*
Requirement:
    To create a program that computes and display initials from  full name and displays them.

Entity:
    ComputeInitial.
    
Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1) A name is assigned to the string variable.
    2) Each String value is splited and assigned to the list.
    3) For Each element of the name 
        3.1)Check Whether the letter at beginning and after space are lower case.
            3.1.1)If true, print the upperCase of the letter
            3.1.2) Otherwise print the character.
*/
package com.kpr.training.string;

public class ComputeInitial {

    public static void main(String[] args) {
        String name = "Gokul D";
        String[] a = name.split(" ");
        System.out.print("My initials are:");
        for (String j : a) {
            if (Character.isLowerCase(j.charAt(0))) {
                System.out.print(Character.toUpperCase(j.charAt(0)) + " ");
            } else {
                System.out.print(j.charAt(0) + " ");
            }
        }
    }
}