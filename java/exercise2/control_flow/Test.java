

/*
 * Requirements: 
 *      To write the test program consisting the following codes snippet and make the value
 * of aNumber to 3 and find the output of the code by using proper line space amd breakers and also
 * by using proper curly braces. The following snippet is given as if (aNumber >= 0) if (aNumber ==
 * 0) System.out.println("first string"); else System.out.println("second string");
 * System.out.println("third string");
 * 
 *      What output do you think the code will produce if aNumber is 3?

        Write a test program containing the previous code snippet
        - and make aNumber 3. What is the output of the program?
        - Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        - Use braces, { and }, to further clarify the code.

 * 
 * Entities: 
 *      Test
 * 
 * Method Signature: 
 *      public static void main(String[] args).
 * 
 * Jobs To Be Done: 
 *      1)Declare the integer variable and assign 3 to it.
 *      2)Check whether the number is greater than or equal to 0.
 *          2.1)if true, then check whether the number is equal to 0.
 *              2.1.1)if true, then print "First String".
 *          2.2)if false, then print "Second String".
 *      3)Print "Third String".
 */

/*
 * Output:
 * 
 * second string
 * third string
 */

package com.kpr.training.control_flow;

public class Test {
    public static void main(String[] args) {
        int aNumber = 3;
        if (aNumber >= 0) {

            if (aNumber == 0) {
                System.out.println("First String");
            }
        } else {
            System.out.println("Second String");
        }
        System.out.println("Third String");
    }
}
