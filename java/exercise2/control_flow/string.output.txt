/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    No Entity .

Function Declaration:
    No function is declared.

Jobs To Be Done:
    1)Considering the given program
    2)Checking the aNumber value is greater than or equal to zero.
    3)It is true, So it moves to the next if statement.
    4)Checking the aNumber value equal to zero 
    5)This statement is false, So it skips the step.
    6)It moves to the else of previous if part.
    7)It prints the second string and also third string.
*/
OUTPUT:
second string
third string