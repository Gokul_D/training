

/*
Requirement:
     To implements the CharSequence interface found in the java.lang package and returning the string backwards.

Entity:
    CharSequenceDemo

Function Declaration:
    public static char charat(int a),
    public static int stringLength(),
    public static void subStrings(int a, int b).

Jobs To Be Done:
    1) Declared the class as CharSequenceDemo.
    2) Declaring the object as words and assigning the variable a String.
    3) Calling the function charat with int variable as 5 and returning the char variable and printing it.
    4)Calling the stringLength function and returning and printing the total lenth of the String.
    5) Calling the subString function with starting and ending value as 7 and 22. And printing the resultant String.
    6) toString helps to print the normal string.
*/

package com.kpr.training.interfacedemo;

public class CharSequenceDemo {
    public static String sentence;

    public static char charat(int a) {
        return sentence.charAt(a);
    }

    public static int stringLength() {
        return sentence.length();
    }

    public static void subStrings(int a, int b) {
        System.out.println(sentence.subSequence(a, b));
    }

    public static void main(String[] args) {
        CharSequenceDemo words = new CharSequenceDemo();
        words.sentence =
                "This program return the CharSequence interface found in the java.lang.package";
        System.out.println(charat(5));
        System.out.println(stringLength());
        subStrings(7, 22);
        System.out.println(sentence.toString());
    }
}
