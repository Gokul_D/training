/*
Requirement:
    To demonstrate overloading with Wrapper types.

Entity:
    WrapperOverLoading

Function Signature:
    public void test(Integer value)
    public void test(Float value)
    public void test(Double value)
    public void test(Long value)
    public static void main(String[] args)

Jobs to be done:
    1.Create a object for the class.
    2.Call the methods with different data type values like integer,float,double,long.
    3.Print the respective values of wrapper types.
*/

//PROGRAM:
package com.kpr.training.datatype;

public class WrapperOverLoading {

    public void test(Integer value) {
        System.out.println("Integer : " + value);
    }

    public void test(Float value) {
        System.out.println("Float : " + value);
    }

    public void test(Double value) {
        System.out.println("Double : " + value);
    }

    public void test(Long value) {
        System.out.println("Long : " + value);
    }

    public static void main(String[] args) {
        WrapperOverLoading value = new WrapperOverLoading();
        value.test(45);
        value.test(9.32f);
        value.test(145.753273645d);
        value.test(12546934L);
    }
}