/*
 * Requirement: 
 *      To create a program that is similar to the previous one but instead of reading
 * integer arguments it reads floating-point arguments.It displays the sum of the arguments, using
 * exactly two digits to the right of the decimal point.For example, suppose that you enter the
 * following: java FPAdder 1 1e2 3.0 4.754 The program would display 108.75. Depending on your
 * locale, the decimal point might be a comma (,) instead of a period (.).
 * 
 * Entity:  
 *      FloatAdder
 * 
 * Method Signature: 
 *      public void add(Float... numbers) 
 *      public static void main(String[] args)
 * 
 * Jobs to be done: 
 *      1)Create a method with vararg of integer type as parameter.
 *          1.1)check whether the parameter length is equal to 1.
 *              1.1.1)if true, then print "Add more numbers"
 *              1.1.2)if false, then 
 *                  1.1.2.1)initialize a float variable sum with value as 0.
 *                  1.1.2.2)for each number in the vararg parameter.
 *                      1.1.2.2.3)add each number to the sum.
 *                  1.1.2.3)Print the value of sum with two decimal values.
 *      2)Invoke the method with n float values as parameter.
 */
package com.kpr.training.datatype;

public class FloatAdder {

    public void add(float... numbers) {
        if (numbers.length == 1) {
            System.out.println("Add more numbers");
        } else {
            float sum = 0;
            for (float number : numbers) {
                sum += number;
            }
            System.out.format("%.2f", sum);
        }
    }

    public static void main(String[] args) {
        FloatAdder adder = new FloatAdder();
        adder.add(1, 1e2f, 3.0f, 4.754f);
        adder.add(4.78f);
    }
}