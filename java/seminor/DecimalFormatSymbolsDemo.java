import java.text.DecimalFormatSymbols;
import java.text.DecimalFormat;
import java.util.Locale;

public class DecimalFormatSymbolsDemo {
    
    public static void main(String[] args) { 
        Locale locale = new Locale("en", "UK");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator(';');
        symbols.setGroupingSeparator(':');
        String pattern = "#,##0.###";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
        String number = decimalFormat.format(123456789.123);
        System.out.println(number);
    }
}//123,456,789.123