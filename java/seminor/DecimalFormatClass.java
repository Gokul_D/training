import java.text.DecimalFormat; 
import java.util.Locale;

public class DecimalFormatClass {
    
    public static void main(String[] args) {         
        DecimalFormat numFormat;
        String number;
        
        // Decimal Format
        
        numFormat = new DecimalFormat("#,###,###");
        number = numFormat.format(5798462);
        System.out.println("1) DecimalFormat with ,: " + number);
        
        numFormat = new DecimalFormat("000.##");
        number = numFormat.format(17.258);
        System.out.println("2) DecimalFormat with . : " + number);
        
        //decimalFormat.applyPattern("##.##");
         
        // two digits before decimal point and 2 digits after
        
        numFormat = new DecimalFormat("00.00");
        number = numFormat.format(8.776);
        System.out.println("3) DecimalFormat with 4 digits: " + number);
         
        // Grouping
        
        numFormat = new DecimalFormat("#,####");
        number = numFormat.format(55689);
        System.out.println("4) Decimal Format with Grouping : " + number);
      
    }

}