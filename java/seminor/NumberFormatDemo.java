import java.text.NumberFormat;
import java.math.RoundingMode; 
import java.util.Locale;

public class NumberFormatDemo {
    
    public static void main(String[] args) { 

        //Formatting Numbers
        
        Locale locale = new Locale("da", "DK");
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        String number = numberFormat.format(158.32);
        System.out.println("Number Formatting :" + number);
    
        //currency Formatting
        
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
        String currency = currencyFormat.format(2547.486);
        System.out.println("Currency Formatting :" + currency);
    
        // Percentage formatting
        
        NumberFormat percentageFormat = NumberFormat.getPercentInstance(locale);
        String percentage = percentageFormat.format(0.89);
        System.out.println("Percentage Formatting :" + percentage);
        
        //Rounding Mode
        
        NumberFormat rounding = NumberFormat.getInstance(new Locale("da", "DK"));
        rounding.setRoundingMode(RoundingMode.HALF_DOWN);
        rounding.setMinimumFractionDigits(1);
        rounding.setMaximumFractionDigits(3);
        String number1 = rounding.format(99.5256);
        System.out.println("Rounding Mode :" + number1);
        
    }
}