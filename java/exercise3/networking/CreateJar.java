/*
 * Requirement:
 *    int nextToken()
 * Entity:
 *    CreateJar
 * Method Signature:
 *    public static void main(String[] args)
 * Jobs to be done:
 *    1) Create a path instance for a file which has to be created.
 *    2) By using createFile method ,file will be created.
 */
package com.kpr.training.networking;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateJar {

    public static void main(String[] args) throws IOException {
        Path sourceFile = Paths.get(
                "C:\\Users\\ADMIN\\eclipse-workspace\\javaee-demo\\exercise3\\com\\kpr\\training\\networking\\create.jar");
        Files.createFile(sourceFile);
        System.out.println("Jar file created successfully ");
    }
}
