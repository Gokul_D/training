/*
 * Requirements : 
 * 		Reading a file using Reader.
 *
 * Entities :
 * 		ReaderEx.
 * Method Signature :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1.Create a reference for FileReader with file as constructor argument.
 *     	2.Till the end of the file
 *          2.1)Read the content of the file.
 *          2.2)Print the content of the file.
 *     	3.Close the created input stream.
 *
 * PseudoCode:
 * 
 * 		class ReaderEx {
 *			public static void main(String args[]) throws Exception {
 *				Reader reader = new FileReader("ReaderEx.txt");
 *				//Read and Print the content of the file.
 *				reader.close();
 *			}
 *		}
 */
package com.kpr.training.java_advanced;

import java.io.FileReader;
import java.io.Reader;

public class ReaderEx {
    
	public static void main(String args[]) throws Exception {
		Reader reader = new FileReader("ReaderEx.txt");
		int character;
		
		while ((character = reader.read()) != -1) {
			System.out.print((char) character);
		}
		
		reader.close();
	}
}
