/*
 * Requirement:
 *    Demonstrate linked hash set to array() method in java
 *     
 * Entity:
 *    LinkedHashSet
 * 
 * Method Signature:
 *    public static void main(String[] args)
 *     
 * Jobs to be Done:
 *    1.Create a reference for LinkedHashSet with type String.
 *        1.1 Add the values to the LinkedHashSet.
 *    2.Print the LinkedHashSet.
 *    3.Convert the LinkedHashSet to Array and store it in array.
 *    4.For each element in array
 *       4.1.Print the elements.
 * 
 * Pseudo Code:
 * class LinkedHashSetDemo {

        public static void main(String[] args) {
            LinkedHashSet<String> set = new LinkedHashSet<>();
            //add the values to the LinkedHashSet
            System.out.println("The Linked Hash Set " + set);
        
            // Convert the LinkedHashSet to Array
            Object[] array = set.toArray();
            
            //Using forEach loop
            System.out.println("The array is");
            for (Object name : array) {
                System.out.println(name + " ");
            }

        }
    }

 */
package com.kpr.training.collections;

import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

    public static void main(String[] args) {
        LinkedHashSet<String> set = new LinkedHashSet<>();
        set.add("Balaji");
        set.add("Boobalan");
        set.add("Gokul");
        set.add("Karthi");
        set.add("Kiruthic");
        System.out.println("The Linked Hash Set " + set);
        Object[] array = set.toArray();
        System.out.println("The array is");
        for (Object name : array) {
            System.out.println(name + " ");
        }

    }
}