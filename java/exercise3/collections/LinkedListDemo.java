/*
 * Requirements:
 *    Create an array list with 7 elements, and create an empty linked list add all elements of the 
 *    array list to linked list ,traverse the elements and display the result.
 * 
 * Entities:
 *    LinkedListDemo
 *
 * Method Signature:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create a ArrayList with type Integer
 *        1.1 Add the seven elements to the ArrayList
 *    2.Print the  ArrayList.
 *    3.Create a reference LinkedList With type as Integer.
 *        3.1 Add all the elements from ArrayList to LinkedList.
 *    4.Print the LinkedList.
 *    
 * Pseudo Code:
 * class LinkedListDemo {

        public static void main(String[] args) {
            ArrayList<Integer> arrayList = new ArrayList<>();
            //Add the seven elements to the ArrayList
            System.out.println("Array list elements are " + arrayList);
            
            //Create the LinkedList
            LinkedList<Integer> linkedList = new LinkedList<>();
            
            //add all the elements from arrayList to LinkedList
            linkedList.addAll(arrayList);
            
            //Using forEach loop
            System.out.println("Linkedlist elements are using forEach");
            for (int elements : linkedList) {
                System.out.print(elements + " ");
            }
            System.out.println();
        }

 }
 */
package com.kpr.training.collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        System.out.println("Array list elements are " + arrayList);
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(arrayList);
        System.out.println("Linkedlist elements are using forEach");
        for (int elements : linkedList) {
            System.out.print(elements + " ");
        }
        System.out.println();
    }

}