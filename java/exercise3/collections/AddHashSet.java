/*
 * Requirements:
 *    To demonstrate the basic add and traversal operation of linked hash set.
 * 
 * Entities:
 *    AddHashSet 
 *
 * Method Signature:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create a reference for LinkedHashSet with type as Integer.
 *    2.Add the elements to the Set.
 *    3.Print the elements of LinkedHashSet.
 *    4.Create a reference for iterator with type as Integer.
 *    5.for each element in the LinkedHashSet
 *      5.1) Print the elements.
 
 *    
 * Pseudo Code:
 *  class AddHashSet {

        public static void main(String[] args) {
            LinkedHashSet<Integer> set = new LinkedHashSet<>();
            //Add the elements to the Set
            System.out.println("Set elements are " + set);
            System.out.println("Set elements are using iterator");
            Iterator<Integer> iterator = set.iterator();
            while (iterator.hasNext()) {
                System.out.print(iterator.next() + " ");
            }
            System.out.println();
        }
    }

*/
package com.kpr.training.collections;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class AddHashSet {

    public static void main(String[] args) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        System.out.println("Set elements are " + set);
        System.out.println("Set elements are using iterator");
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }
}