/*
 * Requirements:
 *    LIST CONTAINS 10 STUDENT NAMES
      krishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and 
      display only names starting with 'A'.
    
 * Entities:
 *    StudentNames
 *    
 * Method Signature:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1)Create a list of type string.
 *      1.1)Add the given Strings .
 *    2)print the list .
 *    3)Change all value to Uppercase .
 *      3.1) print the list .
 *    4)For each elements in list .
 *        4.1) Check the element start with "A". 
 *            4.1.1) If it start with "A" print the element .
 * Pseudocode:
 * 
 * class StudentNames {
 *       public static void main(String[] args) {
 *              
 *       List<String> list = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
                "murugan", "adhithya", "balaji", "vicky", "priya");
 *      System.out.println("list" + list);
 *      //Change all the list values to Uppercase.
 *      System.out.println("Uppercase of the list is" + list);
 *      for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
    }    
 */
package com.kpr.training.collections;

import java.util.List;
import java.util.Arrays;

public class StudentsName {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
                "murugan", "adhithya", "balaji", "vicky", "priya");
        System.out.println("list" + list);
        list.replaceAll(String::toUpperCase);
        System.out.println("Uppercase of the list is" + list);
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}