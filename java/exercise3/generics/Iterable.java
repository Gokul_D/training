package com.kpr.training.generics;
/*Requirements:
 * 		To Find why except iterator() method in iterable interface, are not neccessary to define in the implemented class?
 * 
 * Entity:
 * 		No Entity
 * 
 * Method Signature:
 * 		No Method Signature
 * 
 * Jobs To Be Done:
 * 		1)Finding why only iterator() method have to be defined.
 */
/*Solution:
 *  The Java Iterable interface has three methods of which only one needs to be implemented. The other two have default implementations.
 */