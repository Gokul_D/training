/*Requirement:
 *      To Write a program to filter the Person, who are male and age greater than 21
 * 
 * Entity:
 *      GenderAgePerson
 * 
 * Function signature:
 *      public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *      1)Invoke a method createRoster from the class Person and store it to the List.
 *      2)for each elements in list .
 *          2.1) Check the each person's gender is equal to Male.
 *              2.1.1) Check the age is greater than 21.
 *                  2.1.1.1) Print the person name.
 *      
 *       
 * Pseudocode:
 * 
 * public class GenderAgePerson {
 * 
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
 *      for (Person p : roster) {
 *          if(gender==male){
 *              if(age>21){
 *                    System.out.println(name);
 *              }
 *          }
 *     }
 *   }
 *}              
 */

package com.kpr.training.person;

import java.util.List;

public class GenderAgePerson {
    
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        for (Person p : roster) {
            if (p.getGender() == Person.Sex.MALE) {
                if (p.getAge() > 21) {
                    System.out.println(p.getName());
                }
            }

        }
    }
}