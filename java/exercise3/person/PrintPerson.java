/*
 * Requirement:
 *      Print all the persons in the roster using java.util.Stream<T>#forEach 
 *
 * Entity:
 *      public class PrintPerson
 * 
 * Function Signature:
 *      public static void main(String[] args)
 * 
 * Jobs to be done:
 *      1) Invoke a method createRoster from the class Person and store it to the List.
 *      2) For each elements in the roster list.
 *          2.1)print the elements
 *      
 * Pseudo Code:
 * class PrintPerson {

        public static void main(String[] args) {
            // Referring from Person.java
            List<Person> roster = Person.createRoster();
            //Create a reference Using stream
            //Print the Person using stream
        }
    }
 */
package com.kpr.training.person;

import java.util.List;
import java.util.stream.Stream;

public class PrintPerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}