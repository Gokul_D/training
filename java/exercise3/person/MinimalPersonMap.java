/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T>#map API by referring Person.java
 * 
 * Entity:
 *      MinimalPersonMap
 *
 * Function Signature:
 *      public static <R, T> void main(String[] args)
 *
 * Jobs To Be Done:
 *      1)Invoke a method createRoster from the class Person and store it to the List.
 *      2)Create a ArrayList of type String.
 *          2.1)Get the Person name using stream mapping and store it in name ArrayList.
 *      3)Create a ArrayList of type String.
 *          3.1)Get the mailId using stream mapping and store it in mailId ArrayList .
 *      4)Create minimalname of type String and store the minimal name in name.
 *      5)Create minimalId of type String and store the minimalId in mailId
 *      6)Print the minimalname and minimalId.
 *      
 * Pseudo code:
 * class MinimalPersonMap {

    public static <R, T> void main(String[] args) {
        List<Person> roster = Person.createRoster();
        //Get the person name using stream mapping 
        //Get the mailId using stream mapping
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }
}

 * 
 */
package com.kpr.training.person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MinimalPersonMap {

    public static <R, T> void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = (ArrayList<String>) roster.stream().map(s -> s.getName())
                .collect(Collectors.toList());
        ArrayList<String> mailId = (ArrayList<String>) roster.stream().map(s -> s.getEmailAddress())
                .collect(Collectors.toList());
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }
}
