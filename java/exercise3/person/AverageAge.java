/* Requirement:
 *      Write a program to find the average age of all the Person in the person List
 *
 * Entity:
 *      AverageAge
 * 
 * Method Signature:
 *      public static void main(String[] args)
 * 
 * Jobs to be done:
 *     1) Invoke a method createRoster from the class Person and store it to the List.
 *     2) Get the average age of the persons from the rosterlist and store it in a double variable.
 *     3) Print the average age.
 *     
 * Pseudo Code:
 * class AverageAge {

        public static void main(String[] args) {
            //Referring from Person.java
            List<Person> roster = Person.createRoster();
            //Get the average age and store it in average
            System.out.println("Average age : " + average);

        }
    }

*/
package com.kpr.training.person;

import java.util.List;

public class AverageAge {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        double average = roster.stream().mapToInt(Person::getAge).average().getAsDouble();
        System.out.println("Average age : " + average);

    }
}