package com.kpr.training.date_and_time;

import java.time.LocalDate;
import java.util.Calendar;

/*
Requirement:
    To write a Java program to get the dates 10 days before and after today.
      1. Using Local date
      2. Using calendar
      
Entity:
    LocalDateDemo
    
Function Signature:
    public static void main(String[] args)
    
Jobs to be done:
    1. Get the today's date and store it in today which is the reference of localDate.
    2. Now print today's date(today)
    3. Now print the day 10 days before today and 10 days after today.
    4. An instance is created for Calendar calendar that gets the calendar using current time zone
       and locale of the system.
    5. Now the date 10 days before the current date and after the current date is printed.
    
Pseudo code:
class LocalDateDemo {
    
    public static void main(String[] args) {
        // using local date
        LocalDate today = LocalDate.now();
        System.out.println("The current date is: " + " " + today);
        System.out.println("The day 10 days before the current day is: " + " " + today.plusDays(-10));
        System.out.println("The day 10 days after the current day is: " + " " + today.plusDays(10));
        
        // using calendar
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
        
    }

}
*/

public class LocalDateDemo {
    
    public static void main(String[] args) {
        // using local date
        LocalDate today = LocalDate.now();
        System.out.println("The current date is: " + " " + today);
        System.out.println("The day 10 days before the current day is: " + " " + today.plusDays(-10));
        System.out.println("The day 10 days after the current day is: " + " " + today.plusDays(10));
        
        // using calendar
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
        
    }
}
