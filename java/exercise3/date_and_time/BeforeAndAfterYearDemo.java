package com.kpr.training.date_and_time;

/*
Requirement:
    To write a Java program to get the dates 1 year before and after today.
      
Entity:
    LocalDateDemo
    
Function Signature:
    public static void main(String[] args)
    
Jobs to be done:
    1. An instance is created for Calendar calendar that gets the calendar using current time zone
       and locale of the system.
    2. Now the date 1 year before the current date and after the current date is printed.
    
Pseudo code:
class BeforeAndAfterYearDemo {
    
    public static void main(String[] args) {
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calender.getTime());
    }
}


*/

import java.util.Calendar;

public class BeforeAndAfterYearDemo {
    
    public static void main(String[] args) {
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calender.getTime());
    }
}
