/*
 * Requirements : 
 * 		 write a Lambda expression program with a single method interface
 *   To concatenate two strings
 * Entities :
 * 		MyInterface,
 * 		ConcatenateTwoStrings.
 * Method Signature :
 * 		String concat(String one, String two),
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Creating the string variable and assign values.
 * 		2)Concat the two stings using lambda expression.
 * 		3)the concatinated two string is printed.
 * 
 * Pseudocode:
 * 	
 * interface MyInterface {
 * 
 *     String concat(String one, String two);
 * }
 * class ConcatenateTwoStrings {
 * 
 *     public  static void main(String[] args) {
 *         //declare two string fields
 *         String one = "Kiruthic";
 *         String two = " P";
 *         
 *         //lambda expression to add two strings
 *         MyInterface test = (str, str1) -> str+str1;
 *         
 *         //printing the resultant string
 *         System.out.println(test.concat(one, two));
 *     }
 * }
 */
package com.kpr.training.lambda_expression;

interface MyInterface {

	String concat(String one, String two);
}

public class ConcatenateTwoStrings {

	public static void main(String[] args) {

		String one = "Kiruthic";
		String two = " P";
		MyInterface test = (str, str1) -> str + str1;
		System.out.println(test.concat(one, two));
	}
}