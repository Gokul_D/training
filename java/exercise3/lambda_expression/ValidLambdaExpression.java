/*Requirement:
    To Find which one of these is a valid lambda expression? and why?:
        (int x, int y) -> x+y; or (x, y) -> x + y;

Entities:
    No entities.

Function Signature.
    No function is declared.

Jobs To be Done:
    1)Check the valid expression.

Solution:
    In the given expression, both the expressions are valid expression.
    Because the major difference between two expression is identifier expression. In lambda expression we may or may not include it */