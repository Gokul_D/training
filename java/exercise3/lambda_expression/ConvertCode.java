/*
 * Requirements : 
 * 		Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }
 * Entities :
 * 		Value
 * 		ConvertCode.
 * Method Signature :
 * 		int getValue();
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Define a lambda expression to return the number 5.
 * 		2)Print the returned value.
 * 
 * Pseudocode:
 * 
 * interface Value() {
 * 	  int getValue();
 * }
 * public class ConvertCode {
 * 
 *     public static void main(String[] args) {
 *     	  // lambda expression to return 5.
 * 	      Value test = () -> 5;
 *        //Print the returned value.
 * 		  System.out.println(test.getValue());
 *     }
 * }
 */
package com.kpr.training.lambda_expression;

interface Value {

	int getValue();
}


public class ConvertCode {

	public static void main(String[] args) {
		Value test = () -> 5;
		System.out.println(test.getValue());
	}
}