/*
 * Requirement:
 * 	 Method Reference and its types and To write a sample program for each type.
 * 
 * Entity:
 * 	MethodReference
 * 	1)StaticMethodReference , 2)ParticularInstanceMethodReference , 3)ArbitraryInstanceMethodReference, 	4)ArbitraryInstanceMethodReference - nested class
 * 
 * Method Signature:
 * public static void main(String[] args){}
 * 
 * Jobs To Be Done:
 * 		1)Writing what is method reference and its types.
 * 		2)In general main class, declared number of nested class for its each types.
 * 		3)Input is given according to its method type.
 * 		4)Output is printed in two ways. 
 * 			4.1)One is Method reference type
 * 			4.2)other is Lambda expression type.
 * 
 * Pseudo Code:
 * 		class MethodReference {
 * 			class StaticMethodReference {
 * 				public static void main(String[] args){
	 * 				//input is declared as array list.
	 * 				
	 * 				//printing the list in method reference and lambda expression.
	 * 				list.forEach(StaticMethodReference::print);
	 * 				list.forEach(number -> StaticMethodReference.print(number));
	 * 			}
 * 			}
 * 		
 * 			class ParticularInstanceMethodReference {
 * 
 * 				class MyComparator {
 * 					public int compare(int a, int b) {
 * 						System.out.println(a.compareTo(b));
 * 					}
 * 				}
 * 				public static void main(String[] args){
	 * 				//input is declared as array list.
	 * 				final MyComparator myComparator = new MyComparator();
	 * 
	 * 				//printing the list in method reference and lambda expression.
	 * 				Collections.sort(list, myComparator::compare);
	 * 				Collections.sort(list, (a,b) -> myComparator.compare(a,b));
	 * 			}
 * 			}
 * 
 * 			class ArbitraryInstanceMethodReference {
 * 				public static void main(String[] args) {
 * 					//for method reference the name is listed in string list 
 * 					System.out.println(name);
 * 					
 * 					//for lambda expression the name is declared as array list.
 * 					name.forEach( person -> System.out.println(person));
 * 				}
 * 			}
 * 
 * 			class ArbitraryInstanceMethodReference {
 * 				public static void main(String[] args) {
 * 				 	//input is declared as array list.
 * 
 *	 				//The input is copied to the new list and passed to the function copyElements method reference and lambda 						expression type
 *					copyElements(list, ArrayList<Integer>::new);
 *					copyElements(list, () -> new ArrayList<Integer>());
 * 				}
 * 				void copyElements (list, targetCollection) {
 * 					list.forEach(targetCollection.get()::add);
	        		System.out.println(list);
 * 				}
 * 			}
 * 		}
 */

/*Method references are effectively a subset of lambda expressions, because if a lambda expression can be used, then it might be possible to use a method reference.
 * TYPES: 
 * 		Reference to a static method
 * 		Reference to an instance method of a particular object
 * 		Reference to an instance method of an arbitrary object of a particular type
 * 		Reference to a constructor
*/
package com.kpr.training.lambda_expression;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class MethodReference {
	
	//Reference to a Static Method
	
	public static class StaticMethodReference{
		
		public static void main(String[] args) {
	        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	        // Method reference
	        System.out.println("Method Reference:");
	        list.forEach(StaticMethodReference::print);
	        // Lambda expression
	        System.out.println("Lambda Expression:");
	        list.forEach(number -> StaticMethodReference.print(number));
	        // normal
	        System.out.println("Normal:");
	        for(int number : list) {
	            StaticMethodReference.print(number);
	        }
		}
	    public static void print(final int number) {
	        System.out.print(" " + number);
	    }
	}
	
	//Reference to an Instance Method of a Particular Object
	
	public static class ParticularInstanceMethodReference {
		
	    public static void main(String[] args) {
	        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	        final MyComparator myComparator = new MyComparator();
	        // Method reference
	        System.out.println("Method reference:");
	        Collections.sort(list, myComparator::compare);
	        System.out.println("");
	        // Lambda expression
	        System.out.println("Lambda Expresssion:");
	        Collections.sort(list, (a,b) -> myComparator.compare(a,b));
	    }
	    private static class MyComparator {
	    	
	        public int compare(final Integer a, final Integer b) {
	            System.out.print(a.compareTo(b) + " ");
				
				return a.compareTo(b);
	        }
	    }
	}
	
	//Reference to an Instance Method of an Arbitrary Object of a Particular Type
	
	public static class ArbitraryInstanceMethodReference {
		
	    public static void main(String[] args) {
	        String[] name = {"Kiruthic P", "Navaneethan N", "Karthikeyan C", "Boobalan P", "Balaji Pa", "Gokul D"};
	        // Method reference
	        Arrays.sort(name, String::compareToIgnoreCase);
	        System.out.println("Method Reference");
	        for(String i : name) {
	        	System.out.print(i + " ");
	        }
	        System.out.println("");
	        // Lambda expression
	        List<String> Lname = new ArrayList<>();
	        Lname.add("Kiruthic P");
	        Lname.add("Navaneethan N");
	        Lname.add("Boobalan P");
	        Lname.add("Gokul D");
	        Lname.add("Karthikeyan C");
	        Lname.add("Balaji Pa");
	        System.out.println("Lambda Expression");
	        Lname.forEach(value -> System.out.print(value + " "));
	        
	        
	    }
	    
	}
	
	// Reference to a Constructor
	
	public static class ConstructorMethodReference {
		
	    public static void main(String[] args) {
	        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	        // Method Reference
	        System.out.println("Method Reference:");
	        copyElements(list, ArrayList<Integer>::new);
	        // Lambda expression
	        System.out.println("");
	        System.out.println("Lambda Expression:");
	        copyElements(list, () -> new ArrayList<Integer>());
	    }
	    private static void copyElements(final List<Integer> list, final Supplier<Collection<Integer>> targetCollection) {
	        // Method reference to a particular instance
	        list.forEach(targetCollection.get()::add);
	        System.out.println(list + " ");
	    }
	}
	
}
