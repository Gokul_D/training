/*
Requirement:
    Difference between throws and throw , Explain with simple example.
Entity:
    Example1
    Example2
Function Declaration:
    public static void main(String args[])
    void checkAge(int age)
    int division(int a, int b)

throw:
    ->Difference between throws and throw , Explain with simple example.
    ->Programmers cannot disseminate checked exceptions using the throw keyword.
    ->An instance trails the throw keyword.
    ->You have to use the throw keyword inside any method
    ->Many exceptions cannot be thrown.


Example:
public class Example1{ 
   void checkAge(int age){ 
    if(age<18)  
    throw new ArithmeticException("Not Eligible for voting");  
    else 
        System.out.println("Eligible for voting"); 
   } 
   public static void main(String args[]){  
        Example1 obj = new Example1();
        obj.checkAge(13);  
        System.out.println("End Of Program");  
   }
}

throws:
    ->This keyword is used for declaring any exception.
    ->Programmers can disseminate checked exceptions using throws keyword
    ->A class trails the throws keyword.
    ->You have to use the throws keyword with any sign of the method.
    ->Many exceptions can be declared using the throws.

Example:
public class Example2{  
   int division(int a, int b) throws ArithmeticException{  
    int t = a/b;
    return t;
   }  
   public static void main(String args[]){  
        Example1 obj = new Example1();
        try{
        System.out.println(obj.division(15,0));  
        }
        catch(ArithmeticException e){
            System.out.println("You shouldn't divide number by zero");
        }
   }
}
*/