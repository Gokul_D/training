/*
 * Requirements:
 * Write a program ListOfNumbers using try and catch block.
 * 
 * Entities:
 * 		 ListOfNumbers 
 * 
 * Method Signature:
 * public static void main(String[] args) {}
 * 
 * Jobs To Be Done:
 * 		1) Create a reference for arraylist and assign its length.
 * 		2) Using try catch block the value is assigned to the some index .
 * 			2.1)If the given number is not an integer type, it throws an exception.
 * 			2.2)If the index is greater than declared, it throws an exception.
 * 
 * Pseudo Code:
 * 	class ListOfNumbers {
 * 		public static void main(String[] args) {
 * 			int[] arrayOfNumbers = new int[10];
 * 			try {
 * 				arrayOfNumbers[10] = 16;
 * 			} catch (NumberFormatException exception) {
 * 				System.out.println("Number Format Exception");
 * 			} catch (IndexOutOfBoundException exception) {
 * 				System.out.println("Index out of Bound Exception");
 * 		}
 * 	}
 */
 package com.kpr.training.exception_handling;
public class ListOfNumbers {

	public static void main(String[] args) {
		
		int[] arrayOfNumbers = new int[10];
		
		try {
			arrayOfNumbers[10] = 16;
		} catch (NumberFormatException exception) {
			System.out.println("Number Format Exception " + exception.getMessage());
	    } catch (IndexOutOfBoundsException exception) {
	    	System.out.println("Index Out Of Bounds Exception " + exception.getMessage());
	    }

	}

}