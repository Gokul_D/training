package com.kpr.training.exception_handling;
/*
*Requirement:
     Explain the below program.
        try{
          dao.readPerson();
        } catch (SQLException sqlException) {
        throw new MyException("wrong", sqlException);
        }        
 Explanation:
      The try block execute first, Call the method readPerson by using the object dao.In some case exception occurs while there may be an database access error,then throw the user define exception "wrong".
*/