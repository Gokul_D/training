/*
 * Requirement: 
 * 		To create a stack using generic type and implement 
 * 			1)Push atleast 5 elements.
 * 		    2)Pop the peek element. 
 * 			3)Search a element in stack and print the index value. 
 * 			4)print the size of stack. 
 * 			5)print the elements using stream. 
 * Entity:
 * 		 StackGeneric
 * 
 * Method Signature:
 * 		 public static void main(String[] args)
 * 
 * Jobs To Be Done: 
 * 		1)Create an object for Stack with type as Integer.
 * 		2)Push the elements to the stack. 
 * 		3)The peek element is popped and print it.
 * 		4)Search a particular element in the stack, if it is present, print the index value.
 *      5)Print the size of the stack.
 * 		6)for each element,
 *      	6.1).Print the stack elements.
 * 
 * Pseudo Code:
 * 		class StackGeneric {
 * 			//Create the stack as test 
 * 			Stack<Integer> test = new Stack<>();
 * 			
 * 			//Push some elements to the stack
 * 			test.push(5);
 * 
 * 			//Print the pop element
 * 			System.out.println(test.pop());
 * 			//Print the index of the element
 * 			System.out.println(test.indexOf(5));
 * 			//Print the size of stack
 * 			System.out.println(test.size());
 * 
 * 			//Stream is created for test stack
 * 			//Stack element is printed using stream.
 * 		}
 * 
 */
package com.kpr.training.stack_and_queue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackGeneric {

	public static void main(String[] args) {

		Stack<Integer> test = new Stack<>();
		test.push(12);
		test.push(57);
		test.push(78);
		test.push(83);
		test.push(97);
		System.out.println("The peek element is " + test.peek());
		System.out.println("The poped element is " + test.pop());
		System.out.println("The Stack after poped is " + test);
		System.out.println("The Index of the search element is " + test.indexOf(57));
		System.out.println("The Size of the Stack is " + test.size());

		Stream<Integer> stream = test.stream();
		System.out.print("The Stack elements printed using Stream ");
		stream.forEach(i -> System.out.print(i + " "));
	}

}

