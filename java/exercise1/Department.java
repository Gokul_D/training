package com.department.training.core;


public abstract class Department{
    public String departmentName;
    public int departmentId;
    
    public Department(String name,int id) {
        this.departmentName = name;
        this.departmentId = id;
    }
    
    public abstract void Details();
    
    
    
}