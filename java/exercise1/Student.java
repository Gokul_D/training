package com.school.section.core;

//import com.school.section.student;    //fully qualified name

public class Student {

    private String studentName;

    public int rollNO;

    public Student() {
        //non-parameterized constructor
    }

    protected Student(int code, String name) {         //Parameterized constructor
        rollNO = code;
        studentName = name;
    }
    {
        studentName = "ravi";
        rollNO = 01;
    }
    public void printInfo(){
        System.out.println( " Student Name :" + studentName+ " rollNO: " + rollNO );
    }   
    public static void main(String[] args) {
        Student s1 = new Student();
        Student s2 = new Student(02,"raja");
        s1.printInfo();
        s2.printInfo();
    }
}