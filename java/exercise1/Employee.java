import com.department.training.core.Department;

public class Employee extends Department {
    protected int employeeId;
    
    public Employee(String name, int id, int employeeId) {
        super(name, id);
        this.employeeId = employeeId;
    }
    public void Details() {
        if(employeeId == 0) {
            System.out.println("No such Employee is in our department");
        }
        else {
            System.out.println(this.employeeId + " " + "is in our department");
        }
    }
    
    public static void main(String[] args) {
        Department department = new Employee("raja",1,12);
        Department employee = new Employee("rani",4,5);
        department.Details();
        employee.Details();
    }
}