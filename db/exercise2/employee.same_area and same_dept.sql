SELECT employee.first_name
      ,employee.area AS same_area
	  ,department.dept_name AS same_dept
  FROM employee employee,department department 
  WHERE employee.dept_id = department.dept_id
  AND employee.area IN('selam')
  AND department.dept_name IN('facility');
  