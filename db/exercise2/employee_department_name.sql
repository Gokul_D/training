SELECT employee.first_name 
      ,employee.surname
      ,department.dept_name 
  FROM data.employee employee 
  INNER JOIN data.department department 
  Where employee.dept_id=department.dept_id;
