CREATE VIEW crossjoin AS
SELECT student_table.student_id
	  ,student_table.student_name
      ,subject_table.subject_name
      FROM class.student_table
CROSS JOIN class.subject_table;
show TABLES;
SELECT*FROM crossjoin;