SELECT student_table.student_id
	  ,student_table.student_name
      ,subject_table.subject_name
      FROM class.student_table
CROSS JOIN class.subject_table;

SELECT student_table.student_id
	  ,student_table.student_name
      ,student_table.subject_id
      ,subject_table.subject_name
  FROM class.student_table
LEFT JOIN class.subject_table
ON class.student_table.subject_id = class.subject_table.subject_code;

SELECT student_table.student_id
	  ,student_table.student_name
      ,student_table.subject_id
      ,subject_table.subject_name
  FROM class.student_table
RIGHT JOIN class.subject_table
ON class.student_table.subject_id = class.subject_table.subject_code;

SELECT student_table.student_id
	  ,student_table.student_name
      ,student_table.subject_id
      ,subject_table.subject_name
  FROM class.student_table
INNER JOIN class.subject_table
ON class.student_table.subject_id = class.subject_table.subject_code;
