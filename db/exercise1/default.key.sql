CREATE TABLE class.mark_table(
			 student_id INT NOT NULL 
			,student_name VARCHAR(45) NOT NULL DEFAULT 'gokul'
            ,mark INT NOT NULL 
            ,attendance INT NOT NULL
            ,PRIMARY KEY (student_id)
            ,INDEX student_id USING BTREE (student_id) VISIBLE)
