CREATE DATABASE class;
CREATE TABLE class.mark_table(
			 student_id INT
			,student_name VARCHAR(45)
            ,mark INT
            ,attendance INT );
SHOW COLUMNS FROM class.mark_table;