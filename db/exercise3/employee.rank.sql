SELECT employee.name AS employee_name
	   ,university.university_name
       ,college.name AS college_name
       ,department.dept_name
       ,designation.name AS designation_name
       ,designation.rank AS employee_rank
  FROM education.employee employee 
 INNER JOIN education.designation
    ON employee.desig_id = designation.id
 INNER JOIN education.college_department
    ON employee.cdept_id = college_department.cdept_id
 INNER JOIN education.college
    ON college_department.college_id = college.id
 INNER JOIN education.university
    ON college.univ_code = university.univ_code
 INNER JOIN education.department
    ON college_department.udept_code = department.dept_code
 WHERE college.univ_code = ( SELECT university.univ_code
                               FROM education.university university
                              WHERE university.university_name = 'anna university')
 ORDER BY college.name, designation.rank;