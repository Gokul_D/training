SELECT student.id AS roll_number
       ,student.name AS student_name
       ,student.gender
       ,student.dob
       ,student.email
       ,student.phone
       ,college.name AS college_name
       ,college.city AS college_city
       ,department.dept_name
       ,employee.name AS hod_name
       ,university.university_name
  FROM education.student student
 INNER JOIN education.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN education.department
    ON department.dept_code = college_department.udept_code
 INNER JOIN education.college
    ON college.id = college_department.college_id
 INNER JOIN education.university
    ON college.univ_code = university.univ_code
 INNER JOIN education.employee
    ON employee.cdept_id = college_department.cdept_id
   AND employee.college_id = college.id
 WHERE university.university_name IN ('anna university')
   AND college.city IN ('erode')
   AND employee.desig_id = (SELECT designation.id
                              FROM education.designation
							 WHERE designation.name = 'hod')
 LIMIT 20;