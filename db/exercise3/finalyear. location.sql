SELECT student.id AS roll_number
       ,student.name AS student_name
       ,student.gender
       ,student.dob
       ,student.address AS student_address
       ,student.email
       ,student.phone
       ,college.name AS college_name
       ,college.city AS college_city
       ,department.dept_name
       ,university.university_name
  FROM education.student
 INNER JOIN education.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN education.department
	ON department.dept_code =  college_department.udept_code
 INNER JOIN education.college
    ON college.id =  college_department.college_id
 INNER JOIN education.university
    ON college.univ_code = university.univ_code
 WHERE university.university_name IN ('anna university')
   AND college.city IN ('coimbatore')
   AND student.academic_year = '2018';