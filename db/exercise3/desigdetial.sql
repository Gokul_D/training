SELECT u.univ_code
  ,c.name
  ,u.university_name
  ,c.city,c.state
  ,c.year_opened
  ,d.dept_name
  ,e.name AS hod_name
 FROM university u
  ,college c
  ,department d 
  ,designation de
  ,college_department cd
  ,employee e 
 WHERE c.univ_code = u.univ_code 
 AND u.univ_code = d.univ_code 
 AND cd.college_id = c.id 
 AND cd.udept_code = d.dept_code
 AND e.college_id = c.id 
 AND e.cdept_id = cd.cdept_id 
 AND e.desig_id = de.id 
 AND de.name = 'hod';