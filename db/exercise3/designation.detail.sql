SELECT  hod.code
	    ,hod.college_name
        ,university.university_name
        ,hod.city
        ,hod.state
        ,hod.year_opened
        ,department.dept_name
        ,hod.hod_name
  FROM (SELECT college.code
               ,college.name AS college_name
               ,college.univ_code
               ,college.city
               ,college.year_opened
               ,college.state
               ,employee.name AS hod_name
               ,employee.cdept_id
  FROM  employee AS employee
 INNER  JOIN college AS college
	ON  employee.college_id = college.id
 INNER  JOIN designation AS designation
	ON  employee.id = designation.id) AS hod
 INNER  JOIN university AS  university
	ON  hod.univ_code = university.univ_code
 INNER  JOIN college_department AS college_department
	ON  college_department.cdept_id = hod.cdept_id
 INNER  JOIN department
	ON  college_department.udept_code = department.dept_code
 WHERE  department.dept_name IN ('cse','it');