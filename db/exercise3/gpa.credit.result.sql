SELECT university.university_name
       ,student.id AS roll_number
       ,student.name AS student_name
       ,student.gender
       ,student.dob
       ,student.address
       ,college.name AS college_name
       ,department.dept_name
       ,semester_result.semester
       ,semester_result.grade
       ,semester_result.credits
  FROM education.student student
 INNER JOIN education.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN education.college
    ON college.id = college_department.college_id
 INNER JOIN education.university
    ON university.univ_code = college.univ_code
 INNER JOIN education.syllabus
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN education.department
    ON department.dept_code = college_department.udept_code
 INNER JOIN education.semester_result
    ON student.id = semester_result.stud_id
 ORDER BY college.name 
       ,semester_result.semester ;