 SELECT u.university_name
  ,s.id AS roll_number
  ,s.name AS student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name AS college_name
  ,d.dept_name
  ,sf.amount
  ,sf.paid_year
  ,sf.paid_status
 FROM university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
  ,syllabus sy
  ,semester_fee sf
 WHERE c.univ_code = u.univ_code 
 AND u.univ_code = d.univ_code 
 AND cd.college_id = c.id 
 AND cd.udept_code = d.dept_code
 AND s.college_id = c.id
 AND s.cdept_id = cd.cdept_id
 AND sy.cdept_id = cd.cdept_id
 AND sf.stud_id = s.id
 AND sf.cdept_id = cd.cdept_id
 ORDER BY roll_number ;